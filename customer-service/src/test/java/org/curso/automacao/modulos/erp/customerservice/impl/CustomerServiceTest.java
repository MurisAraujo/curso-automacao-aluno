package org.curso.automacao.modulos.erp.customerservice.impl;

import com.github.javafaker.Faker;
import org.curso.automacao.modulos.erp.customerservice.exceptions.ServiceException;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration()
@SpringBootTest()
@ActiveProfiles("dev")
@ExtendWith({ SpringExtension.class, MockitoExtension.class} )
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Tag("unit-tests")
public class CustomerServiceTest {

    @Autowired
    private CustomerRepository customerRepository;

    @InjectMocks
    @Autowired
    private CustomerService customerService;

    @Test
    public void testSaveCustomerWithValidName() throws ServiceException {

        Customer customer = getCustomer();

        Customer savedCustomer = customerService.save(customer);

        assertEquals(customer.getSalary(), savedCustomer.getSalary(), 0.01);
        assertEquals(customer.getAddress(), savedCustomer.getAddress());
        assertEquals(customer.getEmail(), savedCustomer.getEmail());
        assertEquals(customer.getPhoneNumber(), savedCustomer.getPhoneNumber());
        assertEquals(customer.getName(), savedCustomer.getName());
        assertEquals(customer.getCompany(), savedCustomer.getCompany());
        assertEquals(customer.getCity(), savedCustomer.getCity());
        assertEquals(customer.getState(), savedCustomer.getState());
        assertEquals(customer.getCountry(), savedCustomer.getCountry());
        assertEquals(customer.getZipcode(), savedCustomer.getZipcode());

    }

    @Test
    public void testSaveCustomerWithInvalidName() {

        Faker faker = Faker.instance();

        Customer customer = new Customer();
        customer.setName(faker.name().firstName());

        assertThrowsExactly(ServiceException.class, () ->  customerService.save(customer), "Field name must have at least 10 chars.");

    }

    @Test
    public void testGetAllCountries() {
        List<String> result = customerService.getAllCountries();

        assertEquals(193, result.size());
        assertTrue(result.size() > 0);
    }

    private static Customer getCustomer() {
        Faker faker = Faker.instance();

        Customer customer = new Customer();
        customer.setSalary((float) faker.number().randomDouble(2, 100, 10000));
        customer.setAddress(faker.address().fullAddress());
        customer.setEmail(faker.internet().emailAddress());
        customer.setPhoneNumber(faker.phoneNumber().phoneNumber());

        String name = faker.name().fullName();

        customer.setName(name);
        customer.setCompany(faker.company().name());
        customer.setCity(faker.address().city());
        customer.setState(faker.address().state());
        customer.setCountry(faker.address().country());
        customer.setZipcode(faker.address().zipCode());

        return customer;
    }
}
