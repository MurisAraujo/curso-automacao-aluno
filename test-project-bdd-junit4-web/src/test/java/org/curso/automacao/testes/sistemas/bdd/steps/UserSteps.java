package org.curso.automacao.testes.sistemas.bdd.steps;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.sistemas.actions.UserActions;
import org.curso.automacao.testes.sistemas.actions.UserListActions;
import org.curso.automacao.testes.sistemas.actions.HomeActions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class UserSteps extends BaseSteps {

    @Dado("que eu esteja na página de gerenciamento dos usuarios")
    public void que_eu_esteja_na_página_de_gerenciamento_dos_usuarios() {
        // Abrir o módulo de usuarios
        HomeActions.clickOnUsers();

        // Validar se o módulo de usuarios está aberto
        UserListActions.validateUserListPage();
    }

    @Quando("eu inserir um novo usuario")
    public void eu_inserir_um_novo_usuario() {
        // Abrir o formulário de criação de usuarios
        UserListActions.clickOnCreateNewUser();

        // Validar se o formulário de criação de usuarios foi aberto
        UserActions.validateUserPage();
    }

    @Quando("preencher os dados válidos do usuario")
    public void preencher_os_dados_válidos_do_usuario() {
        // Preencher o formulário
        UserActions.fillForm();
    }

    @Quando("submeter os dados do usuario")
    public void submeter_os_dados_do_usuario() {
        // Submeter o formulário
        UserActions.clickOnSubmit();
    }

    @Então("o usuario deverá ser inserido")
    public void o_usuario_deverá_ser_inserido() {
        // Validar se o usuario foi inserido
        UserActions.validateUserSavedSuccessfully();
    }

    @Quando("eu realizar a busca de um usuario")
    public void eu_realizar_a_busca_de_um_usuario() {
        // Realizar a busca pelo último usuario criado
        UserListActions.search(UserActions.userName);
    }

    @Então("o usuario deverá ser retornado na lista")
    public void o_usuario_deverá_ser_retornado_na_lista() {
        // Valida se existe na lista
        UserListActions.checkValueInResultList(UserActions.userName);
    }

    @Quando("alterar o usuario preenchendo os de dados de alteração")
    public void alterar_o_usuario_preenchendo_os_de_dados_de_alteração() {
        // Clicar no botão de atualizar
        UserListActions.clickOnUpdateButton();

        // Preencher somente o nome com modificação
        UserActions.fillFormWithUpdatedName();
    }

    @Então("o usuario deverá ser alterado com sucesso")
    public void o_usuario_deverá_ser_alterado_com_sucesso() {
        // Validar se o usuario foi inserido
        UserActions.validateUserSavedSuccessfully();

        // Voltar a tela de resultados
        UserActions.clickOnUsers();

        // Realizar a busca pelo último usuario criado
        UserListActions.search(UserActions.userName);

        // Valida se existe na lista
        UserListActions.checkValueInResultList(UserActions.userName);
    }

    @Quando("realizar a exclusão do usuario")
    public void realizar_a_exclusão_do_usuario() {
        // Deleta o registro encontrado
        UserListActions.clickOnDeleteButton();

        // Confirma a exclusão
        UserListActions.acceptAlertDeleteConfirmation();
    }

    @Então("o usuario não deverá ser mais encontrado")
    public void o_usuario_não_deverá_ser_mais_encontrado() {
        // Realizar a busca pelo último usuario criado
        UserListActions.search(UserActions.userName);

        // Verifica se nenhum registro foi encontrado
        UserListActions.validateNoMatchingRecordsFound();
    }
}
