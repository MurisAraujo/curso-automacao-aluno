package org.curso.automacao.testes.sistemas.pages;

import java.time.Duration;
import org.curso.automacao.testes.sistemas.bdd.Runner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserPage extends PageBase {

    @FindBy(xpath = "//*[text()='User Form']")
    private WebElement lblUserForm;

    @FindBy(xpath = "//input[@id='userId']")
    private WebElement txtUserId;

    @FindBy(xpath = "//input[@id='userName1']")
    private WebElement txtName;

    @FindBy(xpath = "//input[@id='userName2']")
    private WebElement txtUserName;

    @FindBy(xpath = "//input[@id='userPassword1']")
    private WebElement txtUserPassword;

    @FindBy(xpath = "//input[@id='userPasswordConfirmation']")
    private WebElement txtUserPasswordConfirmation;

    @FindBy(xpath = "//input[@id='userRoles']")
    private WebElement txtUserRoles;

    @FindBy(xpath = "//input[@id='userStatus']")
    private WebElement chkUserStatus;

    @FindBy(xpath = "//button[@id=\"submit\"]")
    private WebElement btnSubmit;

    @FindBy(xpath = "//div[@id='alert-success']")
    private WebElement lblUserSavedSuccesfully;

    @FindBy(xpath = "//a[text()='Users']")
    private WebElement lnkUsers;

    public void fillForm(String name, String userName, String userPassword, String userPasswordConfirmation, String userRoles, boolean status) {

        if (!name.isEmpty())
            fillText(txtName, name);
        if (userName != null )
            fillText(txtUserName, userName);
        if (!userPassword.isEmpty())
            fillText(txtUserPassword, userPassword);
        if (!userPasswordConfirmation.isEmpty())
            fillText(txtUserPasswordConfirmation, userPasswordConfirmation);
        if (!userRoles.isEmpty())
            fillText(txtUserRoles, userRoles);

        if (status == true) {
            if (!chkUserStatus.isSelected())
                click(chkUserStatus);
        } else {
            if (chkUserStatus.isSelected())
                click(chkUserStatus);
        }
    }

    public void clickOnSubmit() {
        click(btnSubmit);
    }

    public void clickOnUsers() {
        click(lnkUsers);
    }

    public void validatePage() {

        // Espera que o element esteja presente na tela
        new WebDriverWait(Runner.driver.get(), Duration.ofSeconds(20))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()='User Form']")));

    }

}
