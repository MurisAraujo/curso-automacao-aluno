package org.curso.automacao.testes.sistemas.actions;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.sistemas.pages.UserPage;
import org.curso.automacao.testes.sistemas.pages.MasterPageFactory;

import static org.junit.Assert.assertTrue;

public class UserActions {

    public static String userName = StringUtils.EMPTY;
    public static String userId = StringUtils.EMPTY;

    public static UserPage userPage() {
        return MasterPageFactory.getPage(UserPage.class);
    }

    public static void validateUserPage() {
        userPage().validatePage();

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void fillForm() {

        Faker faker = Faker.instance();

        userName = faker.name().fullName();

        String password = faker.internet().password();

        userPage().fillForm(userName,
                faker.internet().emailAddress(),
                password,
                password,
                "ROLE_ADMIN, ROLE_USER",
                true);

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void fillFormWithUpdatedName() {

        Faker faker = Faker.instance();

        userName = faker.name().fullName();

        userPage().fillForm(userName,
                faker.internet().emailAddress(),
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                true);

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void clickOnSubmit() {

        userPage().clickOnSubmit();
        // Colher a evidência da tela
        BaseSteps.takeScreenshot();

        userId = userPage().getTxtUserId().getText();
    }

    public static void clickOnUsers() {

        userPage().clickOnUsers();
        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }


    public static void validateUserSavedSuccessfully() {

        assertTrue("Validate if user saved sucessfully panel is displayed.", userPage().getLblUserSavedSuccesfully().isDisplayed());
    }
}
