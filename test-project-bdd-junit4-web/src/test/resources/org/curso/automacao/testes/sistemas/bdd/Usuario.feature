#language: pt

@env.dev @customers
Funcionalidade: Cadastro de Usuarios
  Como usuário cadastrado e autenticado
  Eu quero gerenciar os usuarios da aplicação
  Para que eles possam enviar pedidos

  Contexto: Autenticação de Usuários na Aplicação
    Dado que eu esteja na página de login da aplicação web
    Quando eu inserir os dados válidos de "admin@automacao.org.br" e "password01" para autenticação na aplicação web
    E confirmar o login na aplicação web
    Então serei direcionado para a página principal da aplicação web

  @system-tests
  Cenário: Inserção de um Novo Usuario
    Dado que eu esteja na página de gerenciamento dos usuarios
    Quando eu inserir um novo usuario
    E preencher os dados válidos do usuario
    E submeter os dados do usuario
    Então o usuario deverá ser inserido

  @system-tests
  Cenário: Busca de usuario existente
    Dado que eu esteja na página de gerenciamento dos usuarios
    Quando eu realizar a busca de um usuario
    Então o usuario deverá ser retornado na lista

  @system-tests
  Cenário: Alteração de usuario existente
    Dado que eu esteja na página de gerenciamento dos usuarios
    Quando eu realizar a busca de um usuario
    E o usuario deverá ser retornado na lista
    E alterar o usuario preenchendo os de dados de alteração
    E submeter os dados do usuario
    Então o usuario deverá ser alterado com sucesso

  @system-tests
  Cenário: Exclusão de usuario existente
    Dado que eu esteja na página de gerenciamento dos usuarios
    Quando eu realizar a busca de um usuario
    E o usuario deverá ser retornado na lista
    E realizar a exclusão do usuario
    Então o usuario não deverá ser mais encontrado

