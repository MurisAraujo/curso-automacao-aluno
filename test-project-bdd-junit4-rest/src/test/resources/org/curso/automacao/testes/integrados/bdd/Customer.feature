#language: pt

@env.dev @customers
Funcionalidade: Cadastro de Clientes
	Eu como gestor da aplicação quero poder gerenciar os clientes através de uma API

	Contexto: Usuário administrador autenticado com sucesso
		Dado que eu tenha um "admin@automacao.org.br" e "password01" válidos para me autenticar na aplicação via API
  	Quando eu preparar a requisição da API
  	E realizar a chamada dessa requisição na API
  	Então a autenticação deverá ser realizada com sucesso.

  @integration-tests
    Cenário: Listar todos os clientes via API
      Dado que existam clientes cadastrados na base de dados
      Quando eu preparar a requisição para listar todos os clientes via API
      E realizar a chamada para listar todos os clientes via API
      Então a lista de clientes deverá ser retornada com sucesso via API

  @integration-tests
    Cenário: Listar todos os países disponíveis via API
      Dado que existam paises cadastrados na base de dados
      Quando eu preparar a requisição para listar todos os países disponíveis via API
      E realizar a chamada para listar todos os países disponíveis via API
      Então a lista de países deverá ser retornada com sucesso via API

  @integration-tests
    Cenário: Buscar um cliente por ID via API
      Dado que exista um cliente com ID válido na base de dados
      Quando eu preparar a requisição para buscar um cliente por ID via API
      E realizar a chamada para buscar um cliente por ID via API
      Então o cliente com ID especificado deverá ser retornado com sucesso via API

  @integration-tests
    Cenário: Cadastro de cliente via API
      Dado que eu tenha as informações válidas para cadastro de um cliente via API
      Quando eu preparar a requisição de cadastro do cliente via API
      E realizar a chamada de cadastro do cliente via API
      Então o cliente deverá ser cadastrado com sucesso via API

  @integration-tests
    Cenário: Atualizar um cliente via API
      Dado que exista um cliente com ID válido na base de dados
      E eu tenha informações válidas para atualizar o cliente via API
      Quando eu preparar a requisição para atualizar o cliente via API
      E realizar a chamada para atualizar o cliente via API
      Então o cliente deverá ser atualizado com sucesso via API

  @integration-tests
    Cenário: Excluir um cliente por ID via API
      Dado que exista um cliente com ID válido na base de dados
      Quando eu preparar a requisição para excluir um cliente por ID via API
      E realizar a chamada para excluir um cliente por ID via API
      Então o cliente com ID especificado deverá ser excluído com sucesso via API
