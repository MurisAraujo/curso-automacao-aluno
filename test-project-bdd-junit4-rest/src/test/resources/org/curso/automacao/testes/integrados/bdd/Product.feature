#language: pt
#Author: Murilo Amurim
@env.dev @products
Funcionalidade: Cadastro de Produtos
  Eu como gestor da aplicação quero poder gerenciar meus produtos através de uma API

  @integration-tests
  Cenário: Listar todos os produtos via API
    Dado que existam produtos cadastrados na base de dados
    Quando eu preparar a requisição para listar todos os produtos via API
    E realizar a chamada para listar todos os produtos via API
    Então a lista de produtos deverá ser retornada com sucesso via API

  @integration-tests
  Cenário: Listar todas as companhias via API
    Dado que existem companhias cadastradas na base de dados
    Quando eu preparar a requisição para listar todas as companhias via API
    E realizar a chamada para listar todos as companhias via API
    Então a lista de companhias deverá ser retornada com sucesso via API
    
  @integration-tests
  Cenário: Buscar produto por ID via API
  	Dado que exista um produto com ID válido para busca na base de dados
  	Quando eu preparar a requisição para buscar o produto por ID via API
  	E realizar a chamada para buscar um produto por ID
  	Então o cliente com ID especificado deverá ser retornado com sucesso
  	
  @integration-tests
  Cenário: Cadastro de produto via API
  	Dado que eu tenha as informações válidas para cadastro de um produto via API
  	Quando eu preparar a requisição de cadastro de produto via API
  	E realizar a chamada de cadastro do produto via API
  	Então o produto deverá ser cadastrado com sucesso via API
  	
  @integration-tests
  Cenário: Atualização de um produto via API
  	Dado que exista ao menos um produto com ID válido para atualizar no banco
  	E eu tenha informações válidas para atualizar o produto via API
  	Quando eu preparar a requisição para atualizar o produto via API
  	E realizar a chamada para atualizar o produto
  	Então o produto deverá ser atualizado com sucesso via API
  	
  @integration-tests
  Cenário: Exclusão de um produto via API
  	Dado que exista ao menos um produto com ID válido para excluir no banco
  	Quando eu preparar a requisição para excluir o produto via API
  	E realizar a chamada para excluir o produto
  	Então o produto deverá ser deletado com sucesso via API
  	
 	@integration-tests
  Cenário: Atualização de estoque de um produto via API
  	Dado que exista ao menos um produto com ID e estoque válido no banco
  	E eu tenha informações válidas para atualizar o estoque do produto via API
  	Quando eu preparar a requisição para atualizar o estoque do produto via API
  	E realizar a chamada para atualizar o estoque do produto
  	Então o estoque do produto deverá ser atualizado com sucesso via API
  	
  	
  	


