package org.curso.automacao.testes.integrados.actions;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.helpers.entities.Product;
import org.curso.automacao.testes.helpers.entities.ProductStock;
import org.curso.automacao.testes.integrados.bdd.Runner;

import com.github.javafaker.Faker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import io.restassured.specification.RequestSpecification;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import io.restassured.response.Response;

public class ProductActions {
	
	
	public static final String PRODUCT_ALL_ENDPOINT = "/api/v1/products/all";
	public static final String COMPANIES_ALL_ENDPOINT = "/api/v1/companies/all";
	public static final String ID_PRODUCT_ENDPOINT = "/api/v1/products/find-by/id/";
	public static final String NAME_PRODUCT_ENDPOINT = "/api/v1/products/find-by/product-name/";
	public static final String PRODUCT_SAVE_ENDPOINT = "/api/v1/products/save";
	public static final String PRODUCT_UPDATE_ENDPOINT = "/api/v1/products/update";
	public static final String PRODUCT_DELETE_ENDPOINT = "/api/v1/products/delete-by/id/";
	public static final String STOCK_UPDATE_ENDPOINT = "/api/v1/products/update-stock";
	
	public static Product getNewProduct() {
		return Product.builder()
				 .name(Faker.instance().commerce().productName())
				 .price((float)Faker.instance().number().randomDouble(1, 1, 100))
				 .status(Faker.instance().random().nextBoolean())
				 .manufacturer(Faker.instance().company().name())
				 .supplier(Faker.instance().company().name())
				 .stock(ProductStock.builder()
						 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
						 			.build())
				 .build();
	}
	
	public static ProductStock getNewStock() {
		return ProductStock.builder()
	 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
	 			.build();
	}
	
	public static RequestSpecification getNewProductRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.body(product)
				.contentType(ContentType.JSON)
				.log().all(true);
				
	}
	
	public static Response requestNewProduct(RequestSpecification requestSpecification) {
		return requestSpecification
				.when()
				.put(PRODUCT_SAVE_ENDPOINT);
	}
	
	public static Product validateNewProductResponse(Product productCreated, Response response) {
		Product product = response.then()
								  .log()
								  .all(true)
								  .assertThat()
								  .statusCode(201).extract().as(Product.class);
		
		assertNotNull("Validar se o produto não é null", product);
		assertNotEquals("Valida se o id é maior que zero", product.getId(), 0);
		assertTrue("Valida se todos os campos estão iguais tirando o id", new ReflectionEquals(productCreated, "id").matches(product));
		
		BaseSteps.logInfo("Id do produto criado [" + product.getId() + "]");
		
		return product;
	}
	
	public static Product createNewProduct() throws Exception {
		
		Product product = ProductActions.getNewProduct();
		
		RequestSpecification requestSpecification = ProductActions.getNewProductRequestSpecification(product);
		
		Response response = ProductActions.requestNewProduct(requestSpecification);
		
		return ProductActions.validateNewProductResponse(product, response);
		
	}
	
	public static Product getProductValid() throws Exception {
		return createNewProduct();
	}
	
	public static Product getUpdateProduct(Product product) {
		product.setName(Faker.instance().commerce().productName());
		product.setPrice((float)Faker.instance().number().randomDouble(1, 1, 100));
		product.setStatus(Faker.instance().random().nextBoolean());
		product.setManufacturer(Faker.instance().company().name());
		product.setSupplier(Faker.instance().company().name());
		product.setStock(ProductStock.builder()
	 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
	 			.build());
		
		return product;
	}
	
	public static RequestSpecification getUpdateProductRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.body(product)
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestUpdateProduct(RequestSpecification requestSpecification) {
		return requestSpecification
					.when()
					.post(PRODUCT_UPDATE_ENDPOINT);
	}
	
	public static Product validateUpdateProductResponse(Product productUpdated, Response resp) {
		Product product = resp.then()
								 .log()
								 .all(true)
								 .assertThat()
								 .statusCode(200).extract().as(Product.class);
		
		assertNotNull("Validar se a resposta é vazia", product);
		assertNotEquals("Valida se foi alterada", product.getId(), productUpdated.getId());
		assertTrue("Valida campo alterado", new ReflectionEquals(productUpdated, "id").matches(product));
		
		BaseSteps.logInfo("Id do produto atualizado [" + product.getId() + "]");
		
		return productUpdated;
	}
	
	public static RequestSpecification getDeleteProductRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestDeleteProduct(RequestSpecification requestSpecification) {
		return requestSpecification
					.when()
					.delete(PRODUCT_DELETE_ENDPOINT);
	}
	
	public static Product validateDeleteProductResponse(Product productDeleted, Response resp) {
		Product product = resp.then()
								 .log()
								 .all(true)
								 .assertThat()
								 .statusCode(200).extract().as(Product.class);
		
		assertNotNull("Validar se a resposta é vazia", product);
		assertNotEquals("Valida se foi deletado", product.getId(), productDeleted.getId());
		
		BaseSteps.logInfo("Id do produto deletado [" + product.getId() + "]");
		
		return productDeleted;
	}
	
	public static RequestSpecification getAllCompaniesRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestAllCompanies(RequestSpecification requestSpecification) {
		return requestSpecification
					.when()
					.get(COMPANIES_ALL_ENDPOINT);
	}
	
	public static List<String> validateAllCompaniesResponse(Response response) {
		List<String> companies = response.then()
										 .log()
										 .all(true)
										 .assertThat()
										 .statusCode(200).extract().as(List.class);
		
		assertNotNull("Valida resposta das companhias", companies);
		assertTrue("valida entidade", companies.size() > 0);
		
		return companies;
	}
	
	public static RequestSpecification getAllProductsRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestAllProducts(RequestSpecification requestSpecification) {
		return requestSpecification
					.when()
					.get(PRODUCT_ALL_ENDPOINT);
	}
	
	public static List<Product> validateAllProductsResponse(Response response) {
		List<Product> product = response.then()
										 .log()
										 .all(true)
										 .assertThat()
										 .statusCode(200).extract().as(List.class);
		
		assertNotNull("Valida resposta das companhias", product);
		assertTrue("valida entidade", product.size() > 0);
		
		return product;
	}
	
	public static RequestSpecification getIdProductsRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.body(product)
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestIdProducts(RequestSpecification requestSpecification, Long id) {
		return requestSpecification
					.when()
					.post(ID_PRODUCT_ENDPOINT + (Long) id);
	}
	
	public static Product validateIdProductsResponse(Product productId, Response response) {
			  Product product = response.then()
										 .log()
										 .all(true)
										 .assertThat()
										 .statusCode(200).extract().as(Product.class);
		
		assertNotNull("Valida resposta das produtos", product);
		assertNotEquals("Valida se foi encontrado", product.getId(), productId.getId());
		
		return product;
	}
	
	public static RequestSpecification getNameProductsRequestSpecification(Product product) throws Exception {
		return given()
				.baseUri(BaseSteps.getUrl("services.products.url"))
				.header("Authorization", Runner.token.get())
				.body(product)
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestNameProducts(RequestSpecification requestSpecification, String name) {
		return requestSpecification
					.when()
					.post(NAME_PRODUCT_ENDPOINT + (String) name);
	}
	
	public static Product validateNameProductsResponse(Product productName, Response response) {
			  Product product = response.then()
										 .log()
										 .all(true)
										 .assertThat()
										 .statusCode(200).extract().as(Product.class);
		
		assertNotNull("Valida resposta das produtos", product);
		assertNotEquals("Valida se foi encontrado", product.getId(), productName.getId());
		
		return product;
	}
	
}
