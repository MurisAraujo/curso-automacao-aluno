package org.curso.automacao.testes.integrados.actions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.helpers.entities.Customer;
import org.curso.automacao.testes.integrados.bdd.Runner;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.github.javafaker.Faker;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class CustomerActions {
	
	public static final String CUSTOMER_SAVE_ENDPOINT = "/api/v1/customers/save";
	public static final String CUSTOMER_UPDATE_ENDPOINT = "/api/v1/customers/update";
	public static final String CUSTOMER_DELETE_ENDPOINT = "/api/v1/customers/delete-by/id/";
	public static final String CUSTOMER_ALL_ENDPOINT = "/api/v1/customers/all";
	public static final String COUNTRIES_ALL_ENDPOINT = "/api/v1/customers/countries/all";
	public static final String ID_CUSTOMER_ENDPOINT = "/api/v1/customers/find-by/id/";
	
	public static Customer getNewCustomer() {
		
		return Customer.builder()
					.address(Faker.instance().address().fullAddress())
					.city(Faker.instance().address().cityName())
					.company(Faker.instance().company().name())
					.country(Faker.instance().country().name())
					.email(Faker.instance().internet().emailAddress())
					.name(Faker.instance().name().fullName())
					.phoneNumber(Faker.instance().phoneNumber().phoneNumber())									
					.salary(Faker.instance().number().numberBetween(5000, 20000))
					.state(Faker.instance().address().state())
					.zipcode(Faker.instance().address().zipCode())
					.build();

			
	}
	
	public static RequestSpecification getNewCustomerRequestSpecification(Customer customer) throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.body(customer)
				.contentType(ContentType.JSON)
				.log().all(true);
	}
	
	public static Response requestNewCustomer(RequestSpecification requestSpecification) {
		return requestSpecification
				.when()
					.put(CUSTOMER_SAVE_ENDPOINT);
	}
	
	public static Customer validateNewCustomerResponse(Customer customerCreated,  Response response) {
		
		Customer customer = response.then()
									.log()
									.all(true)
									.assertThat()
									.statusCode(201).extract().as(Customer.class);

		assertNotNull("Validate if customer response is not null.", customer);
		assertNotEquals("Validate if customer id is greater than zero.", customer.getId(), 0);
		
		assertTrue("Validate if all fiels of customer are equals (except the id).", new ReflectionEquals(customerCreated, "id").matches(customer));
		
		BaseSteps.logInfo("Customer created id [" + customer.getId() + "]");
		
		return customer;
		
	}
	
	public static Customer createNewCustomer() throws Exception {

		BaseSteps.logInfo("Prepare data for new customer.");
		Customer customer = CustomerActions.getNewCustomer();

		BaseSteps.logInfo("Preparing the request specification.");
		RequestSpecification requestSpecification = CustomerActions.getNewCustomerRequestSpecification(customer);

		BaseSteps.logInfo("Executing the request to authentication api [" + BaseSteps.getUrl("services.customer.url")
				+ CustomerActions.CUSTOMER_SAVE_ENDPOINT + "]");
		Response response = CustomerActions.requestNewCustomer(requestSpecification);

		BaseSteps.logInfo("Validate if customer was succesfully created.");
		return CustomerActions.validateNewCustomerResponse(customer, response);
	}

	public static Customer getCustomerValid() throws Exception {
		return createNewCustomer();
	}

	public static Customer getUpdateCustomer(Customer customer) {

		customer.setAddress(Faker.instance().address().fullAddress());
		customer.setCity(Faker.instance().address().cityName());
		customer.setCompany(Faker.instance().company().name());
		customer.setCountry(Faker.instance().country().name());
		customer.setEmail(Faker.instance().internet().emailAddress());
		customer.setName(Faker.instance().name().fullName());
		customer.setPhoneNumber(Faker.instance().phoneNumber().phoneNumber());
		customer.setSalary(Faker.instance().number().numberBetween(5000, 20000));
		customer.setState(Faker.instance().address().state());
		customer.setZipcode(Faker.instance().address().zipCode());

		return customer;
	}

	public static RequestSpecification getUpdateCustomerRequestSpecification(Customer customer) throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.body(customer)
				.contentType(ContentType.JSON)
				.log().all(true);
	}

	public static Response requestUpdateCustomer(RequestSpecification requestSpecification) {
		return requestSpecification
				.when()
				.post(CUSTOMER_UPDATE_ENDPOINT);
	}

	public static Customer validateUpdateCustomerResponse(Customer customerUpdate, Response response) {
		Customer customer = response.then()
				.log()
				.all(true)
				.assertThat()
				.statusCode(200).extract().as(Customer.class);

		assertNotNull("Validar resposta update", customer);
		assertEquals("Validar entidade alterada", customer.getId(), customerUpdate.getId());

		assertTrue("Validar campos alterados", new ReflectionEquals(customerUpdate, "id").matches(customer));

		BaseSteps.logInfo("Cliente alterado id [" + customer.getId() + "]");

		return customerUpdate;
	}


	public static RequestSpecification getDeleteCustomerRequestSpecification(Customer customer) throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}

	public static Response requestDeleteCustomer(RequestSpecification requestSpecification, long id) {
		return requestSpecification
				.when()
				.delete(CUSTOMER_DELETE_ENDPOINT + (int) id);
	}

	public static Customer validateDeleteCustomerResponse(Customer customerDelete, Response response) {
		Customer customer = response.then()
				.log()
				.all(true)
				.assertThat()
				.statusCode(200).extract().as(Customer.class);

		assertNotNull("Validar resposta delete", customer);
		assertEquals("Validar entidade deletada", customer.getId(), customerDelete.getId());

		BaseSteps.logInfo("Cliente deletado id [" + customer.getId() + "]");

		return customerDelete;
	}

	public static RequestSpecification getAllCustomerRequestSpecification() throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}

	public static Response requestAllCustomer(RequestSpecification requestSpecification) {
		return requestSpecification
				.when()
				.get(CUSTOMER_ALL_ENDPOINT);
	}

	public static List<Customer> validateAllCustomerResponse(Response response) {
		List<Customer> customer = response.then()
				.log()
				.all(true)
				.assertThat()
				.statusCode(200).extract().as(List.class);

		assertNotNull("Validar resposta delete", customer);
		assertTrue("Validar entidade deletada", customer.size() > 0);

		BaseSteps.logInfo("Lista de Clientes  [" + customer.toString() + "]");

		return customer;
	}

	public static RequestSpecification getAllCountriesRequestSpecification() throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}

	public static Response requestAllCountries(RequestSpecification requestSpecification) {
		return requestSpecification
				.when()
				.get(COUNTRIES_ALL_ENDPOINT);
	}

	public static List<String> validateAllCountriesResponse(Response response) {
		List<String> countries = response.then()
				.log()
				.all(true)
				.assertThat()
				.statusCode(200).extract().as(List.class);

		assertNotNull("Validar resposta delete", countries);
		assertTrue("Validar entidade deletada", countries.size() > 192);

		BaseSteps.logInfo("Lista de Clientes  [" + countries.toString() + "]");

		return countries;
	}

	public static RequestSpecification getIdCustomerRequestSpecification(Customer customer) throws Exception {
		return  given()
				.baseUri(BaseSteps.getUrl("services.customers.url"))
				.header("Authorization", Runner.token.get())
				.contentType(ContentType.JSON)
				.log().all(true);
	}

	public static Response requestIdCustomer(RequestSpecification requestSpecification, long id) {
		return requestSpecification
				.when()
				.get(ID_CUSTOMER_ENDPOINT + (int) id);
	}

	public static Customer validateIdCustomerResponse(Customer customerId, Response response) {
		Customer customer = response.then()
				.log()
				.all(true)
				.assertThat()
				.statusCode(200).extract().as(Customer.class);

		assertNotNull("Validar busca cliente", customer);
		assertEquals("Validar cliente retornado", customer.getId(), customerId.getId());

		BaseSteps.logInfo("Cliente retornado id [" + customer.getId() + "]");

		return customer;
	}
}
