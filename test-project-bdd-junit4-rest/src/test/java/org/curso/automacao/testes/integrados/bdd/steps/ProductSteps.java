package org.curso.automacao.testes.integrados.bdd.steps;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.helpers.entities.Product;
import org.curso.automacao.testes.integrados.actions.ProductActions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ProductSteps extends BaseSteps{
	
	private Product product;
	
	private RequestSpecification requestSpecification;
	private Response response;
	
	
	@Dado("que existam produtos cadastrados na base de dados")
	public void que_existam_produtos_cadastrados_na_base_de_dados() throws Exception {
		product = ProductActions.getProductValid();
	}
	@Quando("eu preparar a requisição para listar todos os produtos via API")
	public void eu_preparar_a_requisição_para_listar_todos_os_produtos_via_api() throws Exception {
		requestSpecification = ProductActions.getAllProductsRequestSpecification(product);
	}
	@Quando("realizar a chamada para listar todos os produtos via API")
	public void realizar_a_chamada_para_listar_todos_os_produtos_via_api() {
		response = ProductActions.requestAllProducts(requestSpecification);
	}
	@Então("a lista de produtos deverá ser retornada com sucesso via API")
	public void a_lista_de_produtos_deverá_ser_retornada_com_sucesso_via_api() {
		ProductActions.validateAllProductsResponse(response);
	}
	
	
	
	@Dado("que existem companhias cadastradas na base de dados")
	public void que_existem_companhias_cadastradas_na_base_de_dados() {
	    //mock
	}
	@Quando("eu preparar a requisição para listar todas as companhias via API")
	public void eu_preparar_a_requisição_para_listar_todas_as_companhias_via_api() throws Exception {
	    requestSpecification = ProductActions.getAllCompaniesRequestSpecification(product);
	}
	@Quando("realizar a chamada para listar todos as companhias via API")
	public void realizar_a_chamada_para_listar_todos_as_companhias_via_api() {
	    response = ProductActions.requestAllCompanies(requestSpecification);
	}
	@Então("a lista de companhias deverá ser retornada com sucesso via API")
	public void a_lista_de_companhias_deverá_ser_retornada_com_sucesso_via_api() {
	    ProductActions.validateAllCompaniesResponse(response);
	}
	
	
	@Quando("eu preparar a requisição para buscar o produto por ID via API")
	public void eu_preparar_a_requisição_para_buscar_o_produto_por_id_via_api() throws Exception {
	    requestSpecification = ProductActions.getIdProductsRequestSpecification(product);
	}
	@Quando("realizar a chamada para buscar um produto por ID")
	public void realizar_a_chamada_para_buscar_um_produto_por_id() {
	    response = ProductActions.requestIdProducts(requestSpecification, product.getId());
	}
	@Então("o cliente com ID especificado deverá ser retornado com sucesso")
	public void o_cliente_com_id_especificado_deverá_ser_retornado_com_sucesso() {
	    ProductActions.validateIdProductsResponse(product, response);
	}
	
	
	
	@Dado("que eu tenha as informações válidas para cadastro de um produto via API")
	public void que_eu_tenha_as_informações_válidas_para_cadastro_de_um_produto_via_api() {
		product = ProductActions.getNewProduct();
	}
	@Quando("eu preparar a requisição de cadastro de produto via API")
	public void eu_preparar_a_requisição_de_cadastro_de_produto_via_api() throws Exception {
	    logInfo("Preparando a request");
	    requestSpecification = ProductActions.getNewProductRequestSpecification(product);
	}
	@Quando("realizar a chamada de cadastro do produto via API")
	public void realizar_a_chamada_de_cadastro_do_produto_via_api() throws Exception {
	    logInfo("Executando a request [ " + getUrl("services.products.url") + ProductActions.PRODUCT_SAVE_ENDPOINT + "]");
	    
	    response = ProductActions.requestNewProduct(requestSpecification);
	}
	@Então("o produto deverá ser cadastrado com sucesso via API")
	public void o_produto_deverá_ser_cadastrado_com_sucesso_via_api() {
		logInfo("Valida se produto foi criado");
	    ProductActions.validateNewProductResponse(product, response);
	}
	
	
	
	@Dado("que exista ao menos um produto com ID válido para atualizar no banco")
	public void que_exista_ao_menos_um_produto_com_id_válido_para_atualizar_no_banco() throws Exception {
	    product = ProductActions.getProductValid();
	}
	@Dado("eu tenha informações válidas para atualizar o produto via API")
	public void eu_tenha_informações_válidas_para_atualizar_o_produto_via_api() {
	    Product productUpdate = ProductActions.getUpdateProduct(product);
	}
	@Quando("eu preparar a requisição para atualizar o produto via API")
	public void eu_preparar_a_requisição_para_atualizar_o_produto_via_api() throws Exception {
	    requestSpecification = ProductActions.getUpdateProductRequestSpecification(product);
	}
	@Quando("realizar a chamada para atualizar o produto")
	public void realizar_a_chamada_para_atualizar_o_produto() {
	    response = ProductActions.requestUpdateProduct(requestSpecification);
	}
	@Então("o produto deverá ser atualizado com sucesso via API")
	public void o_produto_deverá_ser_atualizado_com_sucesso_via_api() {
	    ProductActions.validateUpdateProductResponse(product, response);
	}
	
	
	
	@Dado("que exista ao menos um produto com ID válido para excluir no banco")
	public void que_exista_ao_menos_um_produto_com_id_válido_para_excluir_no_banco() throws Exception {
		product = ProductActions.getProductValid();
	}
	@Quando("eu preparar a requisição para excluir o produto via API")
	public void eu_preparar_a_requisição_para_excluir_o_produto_via_api() throws Exception {
	    requestSpecification = ProductActions.getDeleteProductRequestSpecification(product);
	}
	@Quando("realizar a chamada para excluir o produto")
	public void realizar_a_chamada_para_excluir_o_produto() {
	    response = ProductActions.requestDeleteProduct(requestSpecification);
	}
	@Então("o produto deverá ser deletado com sucesso via API")
	public void o_produto_deverá_ser_deletado_com_sucesso_via_api() {
	    ProductActions.validateDeleteProductResponse(product, response);
	}
	
	
	
	@Dado("que exista ao menos um produto com ID e estoque válido no banco")
	public void que_exista_ao_menos_um_produto_com_id_e_estoque_válido_no_banco() {
	    
	}
	@Dado("eu tenha informações válidas para atualizar o estoque do produto via API")
	public void eu_tenha_informações_válidas_para_atualizar_o_estoque_do_produto_via_api() {
	    
	}
	@Quando("eu preparar a requisição para atualizar o estoque do produto via API")
	public void eu_preparar_a_requisição_para_atualizar_o_estoque_do_produto_via_api() {
	    
	}
	@Quando("realizar a chamada para atualizar o estoque do produto")
	public void realizar_a_chamada_para_atualizar_o_estoque_do_produto() {
	    
	}
	@Então("o estoque do produto deverá ser atualizado com sucesso via API")
	public void o_estoque_do_produto_deverá_ser_atualizado_com_sucesso_via_api() {
	}
}
