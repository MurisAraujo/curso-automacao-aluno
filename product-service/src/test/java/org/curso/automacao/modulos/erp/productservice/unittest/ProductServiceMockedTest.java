package org.curso.automacao.modulos.erp.productservice.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.curso.automacao.modulos.erp.productservice.impl.Product;
import org.curso.automacao.modulos.erp.productservice.impl.ProductQueryBuilder;
import org.curso.automacao.modulos.erp.productservice.impl.ProductRepository;
import org.curso.automacao.modulos.erp.productservice.impl.ProductService;
import org.curso.automacao.modulos.erp.productservice.impl.ProductStock;
import org.curso.automacao.modulos.erp.productservice.exceptions.ServiceException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.javafaker.Faker;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ActiveProfiles({"dev", "mock"})
public class ProductServiceMockedTest extends BaseTest { 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceMockedTest.class);
	
	private static List<Product> products = new ArrayList<>();
	
	@MockBean
	private ProductQueryBuilder productQueryBuilder;
	
	@MockBean
	private ProductRepository productRepository;
	
	@InjectMocks
	@Autowired
	private ProductService productService;
	
	public static Product createProduct() {
		return Product.builder()
				 .name(Faker.instance().commerce().productName())
				 .price((float)Faker.instance().number().randomDouble(1, 1, 100))
				 .status(Faker.instance().random().nextBoolean())
				 .manufacturer(Faker.instance().company().name())
				 .supplier(Faker.instance().company().name())
				 .stock(ProductStock.builder()
						 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
						 			.build())
				 .build();
	}
	
	@BeforeAll
	public static void setUpClass() {
		for(int i = 0; i < 10; i++) {			
			products.add(createProduct());
			products.get(i).setId(i + 1);
		}
	}
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void validateFindAllProducts() throws ServiceException {
		LOGGER.info("Starting test to find all products.");
		
		doAnswer(invocation -> {
			return products;
		}).when(productService.repository).findAll();
		
		assertTrue(productService.findAll().size() > 0, "Validate if the service has products");
		
		assertNotEquals(0, productService.findAll().size(), "Validate if product list is not empty");
		
		LOGGER.info("Total of products found [ " + productService.findAll().size() + " ]");
		
		LOGGER.info("End of test");
	}

	@Test
	public void validateProductCreation() throws ServiceException {
		LOGGER.info("Starting test of product creation.");
		
		doAnswer(invocation -> {
			
			Product productToAdd = invocation.getArgument(0);
			
			products.add(productToAdd);
			productToAdd.setId(products.size() + 1);
			
			return productToAdd;
			
		}).when(productService.repository).save(any(Product.class));
		
		
		Product product = createProduct(),
		productCreated = productService.save(product);
		
		assertNotNull(productCreated);
		assertNotEquals(0, productCreated.getId(), "Validate if the new product has a valid id.");
		assertNotEquals("", productCreated.getName(), "Validate if the new product has a valid Name.");
		assertNotEquals("", productCreated.getManufacturer(), "Validate if the new product has a valid Manufacturer");
		assertNotEquals("", productCreated.getSupplier(), "Validate if the new product has a valid Supplier");
								 
	}
	
	@Test
	public void validateUpdateProduct() throws ServiceException {
		
		doAnswer(invocation -> {
			return products;
		}).when(productService.repository).findAll();
		
		doAnswer(invocation -> {
			
			Product productToUpdate = invocation.getArgument(0);
			
			if(productToUpdate.getId() == 0) {				
				products.add(productToUpdate);
				productToUpdate.setId(products.size() + 1);
			}else {
				Optional<Product> productFound = products.stream().filter(p -> p.getId() == productToUpdate.getId()).findFirst();
				
				if(productFound.isPresent()) {
					products.set(products.indexOf(productFound.get()), productToUpdate);
				}
			}
			
			return productToUpdate;
			
		}).when(productService.repository).save(any(Product.class));
		
		doAnswer(invocation -> {
			
			Long id = invocation.getArgument(0);
			
			Optional<Product> productFound = products.stream().filter(p -> p.getId() == id).findFirst();
			
			return productFound;
			
		}).when(productService.repository).findById(anyLong());
		
		
		
		Product productToUpdate = productService.findAll().get(1);
		
		productToUpdate.setName(Faker.instance().commerce().productName());
		
		productService.save(productToUpdate);
		
		assertEquals(productService.findById(productToUpdate.getId()).get().getName(), productToUpdate.getName(), "Validate if product was updated");
	}
	
	@Test
	public void validateProductDelete() throws ServiceException {
		
		doAnswer(invocation -> {
			return products;
		}).when(productService.repository).findAll();
		
		doAnswer(invocation -> {
			
			Product productToBeDeleted = invocation.getArgument(0);
			products.remove(productToBeDeleted);
			
			return null;
		}).when(productService.repository).delete(any(Product.class));
		
		doAnswer(invocation -> {
			
			Long id = invocation.getArgument(0);
			
			Optional<Product> productFound = products.stream().filter(p -> p.getId() == id).findFirst();
			
			return productFound;
			
		}).when(productService.repository).findById(anyLong());
		
		
		Product productToBeDeleted = productService.findAll().get(2);
		productService.delete(productToBeDeleted);
		Optional<Product> product = productService.findAll().stream()
				.filter(u -> u.getId() == productToBeDeleted.getId()).findFirst();
		
		assertTrue(!product.isPresent(), "Validate if product was deleted");
	}
}
