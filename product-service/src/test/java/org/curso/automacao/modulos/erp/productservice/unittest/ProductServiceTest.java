package org.curso.automacao.modulos.erp.productservice.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.curso.automacao.modulos.erp.productservice.impl.Product;
import org.curso.automacao.modulos.erp.productservice.impl.ProductRepository;
import org.curso.automacao.modulos.erp.productservice.impl.ProductService;
import org.curso.automacao.modulos.erp.productservice.impl.ProductStock;
import org.curso.automacao.modulos.erp.productservice.impl.helpers.UpdateStockInfo;
import org.curso.automacao.modulos.erp.productservice.exceptions.ServiceException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.javafaker.Faker;

public class ProductServiceTest extends BaseTest { 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceTest.class);
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductService productService;
	
	@Test
	public void validateFindAllProducts() throws ServiceException {
		LOGGER.info("Starting test to find all products.");
		
		assertTrue(productService.findAll().size() > 0, "Validate if the service has products");
		
		assertNotEquals(0, productService.findAll().size(), "Validate if product list is not empty");
		
		LOGGER.info("Total of products found [ " + productService.findAll().size() + " ]");
		
		LOGGER.info("End of test");
	}
	
	@Test
	public void validateProductCreation() throws ServiceException {

		Product product = Product.builder()
								 .name(Faker.instance().commerce().productName())
								 .price((float)Faker.instance().number().randomDouble(1, 1, 100))
								 .status(Faker.instance().random().nextBoolean())
								 .manufacturer(Faker.instance().company().name())
								 .supplier(Faker.instance().company().name())
								 .stock(ProductStock.builder()
										 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
										 			.build())
								 .build(),
		productCreated = productService.save(product);
		
		assertNotNull(productCreated);
		assertNotEquals(0, productCreated.getId(), "Validate if the new product has a valid id.");
		assertNotEquals("", productCreated.getName(), "Validate if the new product has a valid Name.");
		assertNotEquals("", productCreated.getManufacturer(), "Validate if the new product has a valid Manufacturer");
		assertNotEquals("", productCreated.getSupplier(), "Validate if the new product has a valid Supplier");
								 
	}
	
	@Test
	public void validateUpdateProduct() throws ServiceException {
		Product productToUpdate = productService.findAll().get(1);
		productToUpdate.setName(Faker.instance().commerce().productName());
		Product productUpdated = productService.save(productToUpdate);
		
		assertEquals(productToUpdate.getName(), productUpdated.getName(), "Validate if product was updated");
	}
	
	@Test
	public void validateProductDelete() throws ServiceException {
		Product productToBeDeleted = productService.findAll().get(2);
		productService.delete(productToBeDeleted);
		Optional<Product> product = productService.findAll().stream()
				.filter(u -> u.getId() == productToBeDeleted.getId()).findFirst();
		
		assertTrue(!product.isPresent(), "Validate if product was deleted");
	}
	
	@Test
	public void validateFindProductByName() throws ServiceException {
		Product product = new Product();
		
		product.setName("Furacão 2000");
		
		Product productSaved = productService.save(product);
		
		Product productByName = productService.findByProductName(productSaved.getName());
		
		
		assertEquals(productByName.getName(), productSaved.getName());
		
	}
	
	@Test
	public void validateUpdateProductStock() throws ServiceException{
		
		Product productToBeUpdated = productService.findAll().get(2);
		
		UpdateStockInfo stockInfo = new UpdateStockInfo();
		
		stockInfo.setId(productToBeUpdated.getId());
	
		stockInfo.setQuantity(Faker.instance().number().numberBetween(1L, 100L));
		
		productService.updateProductStock(stockInfo);
		
		Optional <Product> updatedProduct = productService.findById(productToBeUpdated.getId()).stream()
					.filter(p -> p.getId() == productToBeUpdated.getId()).findFirst();
		
		Product filterProduct = updatedProduct.get();
		
		assertEquals(filterProduct.getStock().getQuantity(), stockInfo.getQuantity(), "Validate if the quantity was changed");
		
		LOGGER.info("End of test : " + filterProduct.getStock().getQuantity());
		
	}
	
	@Test
	public void validateProductDeleteById() throws ServiceException {
		Product productToBeDeleted = productService.findAll().get(2);
		productService.deleteById(productToBeDeleted.getId());
		Optional<Product> product = productService.findAll().stream()
				.filter(u -> u.getId() == productToBeDeleted.getId()).findFirst();
		
		assertTrue(!product.isPresent(), "Validate if product was deleted");
	}
	
	// Tentei de tudo aqui pra testar o gerar arquivo mas não consegui
	/*public void validateFileGenerate() throws ServiceException, FileNotFoundException, IOException{
		List<Product> fakeProducts = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Product fakeProduct = Product.builder()
					 .name(Faker.instance().commerce().productName())
					 .price((float)Faker.instance().number().randomDouble(1, 1, 100))
					 .status(Faker.instance().random().nextBoolean())
					 .manufacturer(Faker.instance().company().name())
					 .supplier(Faker.instance().company().name())
					 .stock(ProductStock.builder()
							 			.quantity(Faker.instance().number().numberBetween(1L, 100L))
							 			.build())
					 .build();
			fakeProducts.add(fakeProduct);
		}
		
		String filePath = productService.generateFile();
		
		File file = new File(filePath);
		assertTrue(file.exists(), "Validate if file was generated");
		
		StringBuilder content = new StringBuilder();
		try (FileReader reader = new FileReader(file)){
			int c;
			while ((c = reader.read()) != -1) {
				content.append((char) c);
			}
		}
		
		assertNotNull(content.toString(), "Verify if the file is a valid JSON representation");
		
		for (Product fakeProduct : fakeProducts) {
			assertTrue(content.toString().contains(fakeProduct.getName()));
			assertTrue(content.toString().contains(String.valueOf(fakeProduct.getPrice())));
			assertTrue(content.toString().contains(fakeProduct.getSupplier()));
			assertTrue(content.toString().contains(fakeProduct.getManufacturer()));
			assertTrue(content.toString().contains(String.valueOf(fakeProduct.getStock().getQuantity())));
		}
		
		file.delete();
	}*/
}
