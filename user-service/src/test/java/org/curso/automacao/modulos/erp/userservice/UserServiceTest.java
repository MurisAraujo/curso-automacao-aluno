package org.curso.automacao.modulos.erp.userservice;

import com.github.javafaker.Faker;
import org.curso.automacao.modulos.erp.userservice.exceptions.ServiceException;
import org.curso.automacao.modulos.erp.userservice.impl.User;
import org.curso.automacao.modulos.erp.userservice.impl.UserQueryBuilder;
import org.curso.automacao.modulos.erp.userservice.impl.UserRepository;
import org.curso.automacao.modulos.erp.userservice.impl.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ContextConfiguration()
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("dev")
@ExtendWith({ SpringExtension.class, MockitoExtension.class} )
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Tag("unit-tests")
public class UserServiceTest {

    private static Faker faker;

    private static List<User> users;

    static {
        faker = Faker.instance();
        users = new ArrayList<>();
    }

    public static User createNewUser(){
        return User.builder()
                .username(faker.internet().emailAddress())
                .userpass(faker.internet().password(10, 15))
                .name(faker.hobbit().character())
                .roles("USER")
                .status(true)
                .build();
    }

    @BeforeAll
    public static void setupAll(){
        for(int i=0 ; i < faker.number().numberBetween(10, 15); i++){
            User user = createNewUser();
            user.setId(i+1);
            users.add(user);
        }
    }

    @BeforeEach
    public void setup(){
        prepareMocks();
    }

    public void prepareMocks(){
        doAnswer(answer -> {
            User user = answer.getArgument(0);
            if(user.getId() != 0){
                for(int i=0; i < users.size(); i++){
                    if(users.get(i).getId() == user.getId()){
                        users.set(i, user);
                        break;
                    }
                }
            } else {
                user.setId(users.size() +1);
                users.add(user);
            }
            return user;
        }).when(userRepository).save(any(User.class));

        doAnswer(answer -> {

            String userNameToFind = answer.getArgument(0);
            Optional<User> userFound = users.stream()
                    .filter(u -> u.getUsername().equals(userNameToFind))
                    .findFirst();

            return userFound.isPresent()? userFound.get() : null;

        }).when(userQueryBuilder).findUserBy(anyString());

        doAnswer(answer -> users).when(userRepository).findAll();

        doAnswer(answer -> {
            User user = answer.getArgument(0);
            if(user.getId() != 0){
                for(int i=0; i < users.size(); i++){
                    if(users.get(i).getId() == user.getId()){
                        users.remove(i);
                        break;
                    }
                }
            }
            return user;
        }).when(userRepository).delete(any(User.class));


    }

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserQueryBuilder userQueryBuilder;

    @InjectMocks
    @Autowired
    private UserService userService;

    @Test
    @DisplayName("Criação de usuários")
    public void testUserCreation() throws ServiceException {

        int rowsPrevious = users.size();
        User userCreated = userService.save(createNewUser());
        int rowsAfter = users.size();
        assertNotEquals(0, userCreated.getId(), "Validação de um ID de usuário válido.");
        assertTrue(rowsAfter > rowsPrevious, "Validação se a quantidade de linhas aumentou da tabela.");

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    @DisplayName("Criação de usuários com nome de usuário duplicado")
    public void testNewUserWithDuplicatedUsername() throws ServiceException {
        User newUser = createNewUser(),
                oldUser = userService.findAll().get(1);

        newUser.setUsername(oldUser.getUsername());

        assertThrowsExactly(ServiceException.class, () -> userService.save(newUser), "Validação da exceção experada");
    }

    @Test
    @DisplayName("Criação de usuário com senha de tamanho inválido")
    public void testNewUserWithPasswordWithInvalidLength() throws ServiceException {
        User newUser = createNewUser();
        newUser.setUserpass(faker.internet().password(1, 9));

        assertThrowsExactly(ServiceException.class, () -> userService.save(newUser), "Validação da exceção experada");
    }

    @Test
    @DisplayName("Atualização de usuário")
    public void testUserUpdate() throws ServiceException {
        User userToUpdate = userService.findAll().get(0);
        userToUpdate.setName(faker.name().fullName());
        User userUpdated = userService.save(userToUpdate);

        assertEquals(userToUpdate.getName(), userUpdated.getName(), "Validação da alteração do nome do usuário");
    }

    @Test
    @DisplayName("Deleção de usuario")
    public void testUserDelete() throws ServiceException {

        User userToBeDeleted = userService.findAll().get(1);

        userService.delete(userToBeDeleted);
        Optional<User> user = userService.findAll().stream()
                .filter(u -> u.getId() == userToBeDeleted.getId()).findFirst();

        assertTrue(!user.isPresent(), "Valida se o usuário foi deletado");

    }

    @Test
    @DisplayName("Buscar todos os usuarios")
    public void testUserFindAll() throws ServiceException {
        assertNotEquals(0, userService.findAll().size(), "Valida se a lista de usuários não está vazia.");
    }


}

